from typing import Optional
from fastapi.responses import PlainTextResponse
from fastapi import FastAPI

import redis
import json

app = FastAPI()

REDIS_HOST = 'redis'
REDIS_PORT = 6379
try:
    r = redis.Redis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        ssl=False)
    print(r)
    r.ping()
    print('Connected')
except Exception as ex:
    print('Error:', ex)
    exit('Failed to connect, terminating.')

@app.get("/")
async def read_root():
    return {"Hello": "World from FASTAPI"}

@app.get("/metrics", response_class=PlainTextResponse)
async def read_metrics():
    cpuTemp = r.get('py:cpu_temp:eb7cb373-af76-49f0-87bb-c614a21c46f9')
    cpuTemp = json.loads(cpuTemp)

    memInfo = r.get('py:mem_info:eb7cb373-af76-49f0-87bb-c614a21c46f9')
    memInfo = json.loads(memInfo)

    multiline_string = (f"cpuTemp {cpuTemp['cpu_temp']}\n"
                    f"MemTotal {memInfo['MemTotal']}\n"
                    f"MemAvailable {memInfo['MemAvailable']}\n"
                    f"MemFree {memInfo['MemFree']}\n"
                    f"Active {memInfo['Active']}\n"
                    f"Inactive {memInfo['Inactive']}\n")
    return multiline_string

@app.get("/prices", response_class=PlainTextResponse)
async def read_metrics():
    ETH = r.get('ETHUSD')
    ETH = json.loads(ETH)

    BTC = r.get('BTCUSD')
    BTC = json.loads(BTC)
    
    SOL = r.get('SOLUSD')
    SOL = json.loads(SOL)

    MATIC = r.get('MATICUSD')
    MATIC = json.loads(MATIC)

    AVAX = r.get('AVAXUSD')
    AVAX = json.loads(AVAX)

    
    multiline_string = (f"ETHUSD {ETH}\n"
                    f"BTCUSD {BTC}\n"
                    f"SOLUSD {SOL}\n"
                    f"MATICUSD {MATIC}\n"
                    f"AVAXUSD {AVAX}\n")
    return multiline_string


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}