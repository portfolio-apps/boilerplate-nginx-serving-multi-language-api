extern crate paho_mqtt as mqtt;
extern crate redis;
extern crate uuid;
extern crate dotenv;
extern crate serde_json;

use dotenv::dotenv;
use std::{
    env,
    process,
    thread,
    time::Duration
};
use redis::Commands;
use uuid::Uuid;

const DFLT_BROKER:&str = "tcp://3.142.247.111:1883";
const DFLT_TOPICS:&[&str] = &[
    "cpu_temp/#",
    "uptime/#",
    "mem_info/#",
    // "lsusb/eb7cb373-af76-49f0-87bb-c614a21c46f9"
];
// The qos list that match topics above.
const DFLT_QOS:&[i32] = &[
    0,
    0,
    0,
    // 0
];

// Reconnect to the broker when connection is lost.
fn try_reconnect(cli: &mqtt::Client) -> bool
{
    println!("Connection lost. Waiting to retry connection");
    for _ in 0..12 {
        thread::sleep(Duration::from_millis(5000));
        if cli.reconnect().is_ok() {
            println!("Successfully reconnected");
            return true;
        }
    }
    println!("Unable to reconnect after several attempts.");
    false
}

fn set_redis_key(key: &str, value: Option<f64>) -> redis::RedisResult<isize> {
    // connect to redis
    let redis_url = env::var("REDIS_URL").expect("REDIS_URL doesn't work or doesn't exist");
    let client = redis::Client::open(redis_url)?;
    let mut con = client.get_connection()?;
    // throw away the result, just make sure it does not fail
    let _ : () = con.set(key, value)?;
    // read back the key and return it.  Because the return value
    // from the function is a result for integer this will automatically
    // convert into one.
    con.get(key)
}

// Subscribes to multiple topics.
fn subscribe_topics(cli: &mqtt::Client) {
    if let Err(e) = cli.subscribe_many(DFLT_TOPICS, DFLT_QOS) {
        println!("Error subscribes topics: {:?}", e);
        process::exit(1);
    }
}

fn show_uuid(uuid: &Uuid) {
    // println!("bytes: {:?}", uuid.as_bytes());
    // println!("simple: {}", uuid.to_simple());
    println!("hyphenated UUID: {}", uuid.to_hyphenated());
    // println!("urn: {}", uuid.to_urn());
}

fn main() {
    dotenv().ok();

    let mqtt_username = env::var("MQTT_USERNAME").expect("MQTT_USERNAME doesn't work or doesn't exist");
    let mqtt_pass = env::var("MQTT_PASSWORD").expect("MQTT_PASSWORD doesn't work or doesn't exist");
    let host = env::args().nth(1).unwrap_or_else(||
        DFLT_BROKER.to_string()
    );

    // Generate a new UUID
    let uuid = Uuid::new_v4();
    show_uuid(&uuid);

    // Define the set of options for the create.
    // Use an ID for a persistent session.
    let create_opts = mqtt::CreateOptionsBuilder::new()
        .server_uri(host)
        .client_id(uuid.to_string())
        .finalize();

    // Create a client.
    let mut cli = mqtt::Client::new(create_opts).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    println!("{:?}", DFLT_TOPICS);

    // Initialize the consumer before connecting.
    let rx = cli.start_consuming();

    // Define the set of options for the connection.
    let lwt = mqtt::MessageBuilder::new()
        .topic("test")
        .payload("Consumer lost connection")
        .finalize();
    let conn_opts = mqtt::ConnectOptionsBuilder::new()
        .user_name(mqtt_username)
        .password(mqtt_pass)
        .keep_alive_interval(Duration::from_secs(20))
        .clean_session(false)
        .will_message(lwt)
        .finalize();

    // Connect and wait for it to complete or fail.
    if let Err(e) = cli.connect(conn_opts) {
        println!("Unable to connect:\n\t{:?}", e);
        process::exit(1);
    }

    // Subscribe topics.
    subscribe_topics(&cli);

    println!("Processing requests...");
    for msg in rx.iter() {
        if let Some(msg) = msg {
            let payload = String::from_utf8_lossy(msg.payload());
            let obj: serde_json::Value = serde_json::from_str(&payload).expect("Unable to parse");
            let _redis_key = set_redis_key(msg.topic(), obj["cpu_temp"].as_f64());
            let _redis_key = set_redis_key(msg.topic(), obj["MemAvailable"].as_f64());
            let _redis_key = set_redis_key(msg.topic(), obj["MemFree"].as_f64());

            println!("{}", "");
            println!("Topic: {}", msg.topic());
            println!("cpu_temp: {:?}", obj["cpu_temp"].as_f64());
            println!("MemAvailable: {:?}", obj["MemAvailable"].as_f64());
            println!("MemFree: {:?}", obj["MemFree"].as_f64());
        }
        else if !cli.is_connected() {
            if try_reconnect(&cli) {
                println!("Resubscribe topics...");
                subscribe_topics(&cli);
            } else {
                break;
            }
        }
    }

    // If still connected, then disconnect now.
    if cli.is_connected() {
        println!("Disconnecting");
        cli.unsubscribe_many(DFLT_TOPICS).unwrap();
        cli.disconnect(None).unwrap();
    }
    println!("Exiting");
}