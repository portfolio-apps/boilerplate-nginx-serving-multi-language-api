extern crate paho_mqtt as mqtt;

use std::process;
use std::time::Duration;
use std::thread::sleep;

const HOST: &str = "tcp://3.142.247.111:1883";
const USERNAME: &str = "username";
const PASSWORD: &str = "WhatUpAndShit";

fn main() {
    // Create a client & define connect options
    let cli = mqtt::AsyncClient::new(HOST).unwrap_or_else(|err| {
        println!("Error creating the client: {}", err);
        process::exit(1);
    });

    let conn_opts = mqtt::ConnectOptionsBuilder::new()
        .user_name(USERNAME)
        .password(PASSWORD)
        .finalize();

    // Connect and wait for it to complete or fail
    if let Err(e) = cli.connect(conn_opts).wait() {
        println!("Unable to connect: {:?}", e);
        process::exit(1);
    }

    let mut counter = 0;

    loop {
        // Create a message and publish it
        println!("Publishing a message on the 'test {}' topic", counter);

        let new_string = format!("My Rust Counter: {}", counter);
        
        let msg = mqtt::Message::new("rust/test", new_string, 0);
        sleep(Duration::from_millis(200));

        let tok = cli.publish(msg);
        counter = counter + 1;
        if let Err(e) = tok.wait() {
            println!("Error sending message: {:?}", e);
        }
    }
}