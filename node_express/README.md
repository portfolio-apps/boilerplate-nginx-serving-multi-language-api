# Run to build services [Express]
<pre>
docker-compose up --build
</pre>

#  You can run the services individually on your host machine instead of docker
### Sequelize Credentials
https://sequelize.org/master/manual/getting-started.html

### Sequelize Migrations
https://sequelize.org/master/manual/migrations.html

### Digital Article for AuthTokens
https://www.digitalocean.com/community/tutorials/nodejs-jwt-expressjs
