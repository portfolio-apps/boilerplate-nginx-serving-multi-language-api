-- Create Databases
CREATE DATABASE IF NOT EXISTS `express`;
CREATE DATABASE IF NOT EXISTS `express_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'express'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;