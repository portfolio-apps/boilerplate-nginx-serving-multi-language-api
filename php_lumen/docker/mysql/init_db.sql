-- Create Databases
CREATE DATABASE IF NOT EXISTS `ngauge`;
CREATE DATABASE IF NOT EXISTS `ngauge_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'ngauge'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;