from paho.mqtt import client as mqtt_client
import asyncio
import random
import time
import redis
import logging
import os
from dotenv import load_dotenv

load_dotenv()

BROKER = os.getenv('MQTT_HOST')
PORT = int(os.getenv('MQTT_PORT'))
USERNAME = os.getenv('MQTT_USERNAME')
PASSWORD = os.getenv('MQTT_PASSWORD')
TOPIC = os.getenv('TOPIC')

client_id = f'python-mqtt-{random.randint(0, 1000)}'

REDIS_HOST = 'redis'
REDIS_PORT = 6379
try:
    conn = redis.Redis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        ssl=False)
    print(conn)
    conn.ping()
    print('Connected')
except Exception as ex:
    print('Error:', ex)
    exit('Failed to connect, terminating.')



###############################################################
##
###############################################################

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(USERNAME, PASSWORD)
    client.on_connect = on_connect
    client.connect(BROKER, PORT)
    return client

###############################################################
##
###############################################################

def publish(client):
    msg_count = 0
    while True:
        time.sleep(1)
        msg = f"messages: {msg_count}"
        result = client.publish(TOPIC, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{TOPIC}`")
        else:
            print(f"Failed to send message to topic {TOPIC}")
        msg_count += 1

###############################################################
##
###############################################################

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        # print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        redis_key = str(msg.topic)
        conn.set("py:" + redis_key.replace("/", ":"), msg.payload.decode())

    client.subscribe(TOPIC)
    client.on_message = on_message

###############################################################
##
###############################################################

def runMqtt():
    client = connect_mqtt()
    # publish(client)
    subscribe(client)
    client.loop_forever()


if __name__ == "__main__":
    runMqtt()