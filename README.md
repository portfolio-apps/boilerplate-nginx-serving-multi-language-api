# Run to build services [Redis, Grafana, Prometheus, Actix, Nginx]
<pre>
docker-compose up --build
</pre>

- Actix Nginx Microservice available on localhost:8080
- PGAdmin available on localhost:8082
    - Username: admin@admin.com
    - Password: postgres
- Prometheus available on localhost:9090
    - Username: admin
    - Password: test1234
- Grafana available on localhost:3000
    - Username: admin
    - Password: admin

#  You can run the services individually on your host machine instead of docker

### To run subscribe
<pre>
cd mqtt
cargo run --bin sub
</pre>

### To publish (Optional)
<pre>
cd mqtt
cargo run --bin pub
</pre>

### Run Actix Server for Node Exporter (Available on locahost:8080)
<pre>
bash scripts/dev.sh
</pre>

### Connect to Redis CLI from within container
<pre>
docker exec -it redis /bin/sh
redis-cli -h 127.0.0.1 -p 6379
</pre>

### Use ngrok to scrape the Actix server, Prometheus has some trouble using docker to scrape host
<pre>
ngrok http 8080
</pre>
