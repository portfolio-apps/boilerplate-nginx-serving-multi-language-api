// src/main.rs
extern crate redis;
#[macro_use]
extern crate log;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use actix_web::{App, HttpResponse, HttpServer, Responder, get};
use actix_web::middleware::Logger;
use env_logger::Env;

use redis::Commands;

mod api_error;
mod db;
mod schema;
mod user;

fn get_redis_key(key: &str) -> redis::RedisResult<f64> {
    // connect to redis
    // let client = redis::Client::open("redis://localhost:6379")?;
    let client = redis::Client::open("redis://redis")?;
    let mut con = client.get_connection()?;
    con.get(key)
}

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Welcome to Actix Rust hello Page!")
}

#[get("/about")]
async fn about() -> impl Responder {
    HttpResponse::Ok().body("This is the Rust Microservice About Page")
}

#[get("/metrics")]
async fn metrics() -> impl Responder {
    let temp = get_redis_key("cpu_temp/eb7cb373-af76-49f0-87bb-c614a21c46f9");
    println!("{:?}", temp);
    let t = format!("{} {:?}\n", "cpu_temp", temp.unwrap());
    HttpResponse::Ok().body(t)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    db::init();

    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(about)
            .service(metrics)
            .configure(user::init_routes)
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
    })
        .bind("0.0.0.0:5000")?
        .run()
        .await
}